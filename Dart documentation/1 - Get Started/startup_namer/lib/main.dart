import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

// => É uma notação para funções ou métodos
void main() => runApp(MyApp());

// StatelessWidget define Widgets que não mudam suas características com a mudança de estado
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Gerar palavras aleatórias
    final wordPair = WordPair.random();
    return MaterialApp(
      title: 'Welcome to Flutter',
      // widget Scaffold, fornece uma barra de aplicativos, um título e uma propriedade body que contém a árvore do widget para a tela inicial
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Flutter'),
        ),
        // O widget Center alinha sua subárvore widget ao centro da tela
        body: Center(
          // Caso Pascal significa que cada palavra na string, incluindo a primeira, começa com uma letra maiúscula
          child: Text(wordPair.asPascalCase),
        ),
      ),
    );
  }
}
